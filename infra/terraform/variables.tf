variable "name" {
  type = string
  default = "imdbgraph-api"
}

variable "resource_group_name" {
  type    = string
  default = "imdbgraph-rg"
}

variable "resource_group_location" {
  type    = string
  default = "eastus"
}